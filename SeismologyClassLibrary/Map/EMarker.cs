﻿namespace SeismologyClassLibrary.Map
{
    /// <summary>
    /// Представляет маркер землетрясения для статичных карт (картинки).
    /// Требует наличия карты
    /// </summary>
    public class EMarker
    {
        /// <summary>
        /// Карта, для которых строится маркер
        /// </summary>
        public static EMap Map { get; set; }

        private double _lat;
        /// <summary>
        /// Широта (в градусах)
        /// </summary>
        public double Lat
        {
            get => _lat;
            set
            {
                _lat = value;
                _latPx = Map.LatEndPx - ((float) value - Map.LatStart) * Map.LatPx;
            }
        }

        private double _lng;
        /// <summary>
        /// Долгота (в градусах)
        /// </summary>
        public double Lng
        {
            get => _lng;
            set
            {
                _lng = value;
                _lngPx = Map.LngStartPx + ((float) value - Map.LngStart) * Map.LngPx;
            }
        }

        private float _latPx;
        /// <summary>
        /// Широта (в пикселях)
        /// </summary>
        public float LatPx => _latPx;

        private float _lngPx;
        /// <summary>
        /// Долгота (в пикселях)
        /// </summary>
        public float LngPx => _lngPx;

        /// <summary>
        /// Инициализирует маркер со следующими значенниями
        /// </summary>
        /// <param name="lat">Широта</param>
        /// <param name="lng">Долгота</param>
        public EMarker(double lat, double lng)
        {
            Lat = lat;
            Lng = lng;
        }

        /// <summary>
        /// Инициализирует маркер со следующими координатами
        /// </summary>
        /// <param name="coordinate">Координаты</param>
        public EMarker(Coordinate coordinate)
        {
            Lat = coordinate.Lat;
            Lng = coordinate.Lng;
        }
    }
}
