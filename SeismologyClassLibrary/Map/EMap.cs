﻿using System.Drawing;

namespace SeismologyClassLibrary.Map
{
    /// <summary>
    /// Карта (обычное изображение) для землетрясений (Дагестана)
    /// </summary>
    public class EMap
    {
        /// <summary>
        /// Изображение карты
        /// </summary>
        public Image Image { get; }

        #region Широта

        /// <summary>
        /// С какого значения широты (в пикселях) начинается отчет
        /// </summary>
        public int LatStartPx { get; }

        /// <summary>
        /// Где заканчивается градусная мера широты в пикселях
        /// </summary>
        public int LatEndPx { get; }

        /// <summary>
        /// В каком значении широты (в пикселях) заканчивается отчет
        /// </summary>
        public int LatLengthPx { get; }

        /// <summary>
        /// С какого значения широты (в градусах) начинается отчет
        /// </summary>
        public float LatStart { get; }

        /// <summary>
        /// В каком значении широты (в градусах) заканчивается отчет
        /// </summary>
        public float LatEnd { get; }

        /// <summary>
        /// 1 градус широты в пикселях
        /// </summary>
        public float LatPx { get; }

        /// <summary>
        /// 1 градус широты в км
        /// </summary>
        public float LatKm { get; }

        #endregion

        #region Долгота

        /// <summary>
        /// С какого значения долготы (в пикселях) начинается отчет
        /// </summary>
        public int LngStartPx { get; }

        /// <summary>
        /// Где заканчивается градусная мера долготы в пикселях
        /// </summary>
        public int LngEndPx { get; }

        /// <summary>
        /// В каком значении долготы (в пикселях) заканчивается отчет
        /// </summary>
        public int LngLengthPx { get; }

        /// <summary>
        /// С какого значения долготы (в градусах) начинается отчет
        /// </summary>
        public float LngStart { get; }

        /// <summary>
        /// В каком значении долготы (в градусах) заканчивается отчет
        /// </summary>
        public float LngEnd { get; }

        /// <summary>
        /// 1 градус долготы в пикселях
        /// </summary>
        public float LngPx { get; }

        /// <summary>
        /// 1 градус долготы в км
        /// </summary>
        public float LngKm { get; }

        #endregion

        /// <summary>
        /// Инициализирует объект класса
        /// </summary>
        /// <param name="image">Изображение</param>
        /// <param name="latStartPx">Начало широты в пикселях</param>
        /// <param name="latEndPx">Конец широты в пикселях</param>
        /// <param name="latStart">Начало широты в градусах</param>
        /// <param name="latEnd">Конец широты в градусах</param>
        /// <param name="lngStartPx">Начало долготы в пикселях</param>
        /// <param name="lngEndPx">Конец долготы в пикселях</param>
        /// <param name="lngStart">Начало долготы в градусах</param>
        /// <param name="lngEnd">Конец долготы в градусах</param>
        /// <param name="latKm">1 градус широты в км</param>
        /// <param name="lngKm">1 градус долготы в км</param>
        public EMap(Image image,
            int latStartPx, int latEndPx,
            float latStart, float latEnd,
            int lngStartPx, int lngEndPx, 
            float lngStart, float lngEnd,
            float latKm = 111.1f, float lngKm = 80.5f)
        {
            Image = image;

            LatStartPx = latStartPx;
            LatEndPx = latEndPx;
            LatLengthPx = latEndPx - latStartPx;
            LatStart = latStart;
            LatEnd = latEnd;
            LatPx = LatLengthPx / (latEnd - latStart);
            LatKm = latKm;

            LngStartPx = lngStartPx;
            LngEndPx = lngEndPx;
            LngLengthPx = lngEndPx - lngStartPx;
            LngStart = lngStart;
            LngEnd = lngEnd;
            LngPx = LngLengthPx / (lngEnd - lngStart);
            LngKm = lngKm;
        }
    }
}
