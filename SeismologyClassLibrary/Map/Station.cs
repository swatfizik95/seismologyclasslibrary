﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using GMap.NET;

namespace SeismologyClassLibrary.Map
{
    /// <summary>
    /// Представляет станцию
    /// </summary>
    public class Station : ICloneable
    {
        // Имена для станций идут следующим образом
        // 1) - Та, что будет в Бюллетене (по идее, рег названия)
        // 2) - Та, что будет в Бюллетене ИФЗ (по идее, международные названия)
        // 3) - Та, что будет в БД ()
        /// <summary>
        /// Станции Дагестана
        /// </summary>
        public static List<Station> Stations { get; set; } = new List<Station>
        {
            new Station("AKT", "AHT", "AKT", @"Ахты", new Coordinate(41.4792, 47.715), Color.FromArgb(219,112,147)),
            new Station("ARKR", "ARK", "ARKR", @"Аракани", new Coordinate( 42.6021, 46.9942), Color.FromArgb(250,128,114)),
            new Station("BTLR", "BTL", "BTLR", @"Ботлих", new Coordinate(42.6653, 46.219), Color.FromArgb(255, 0, 0)),
            new Station("BUJR", "BUJ", "BUJR", @"Буйнакск", new Coordinate(42.8090, 47.1301), Color.FromArgb(128,0,0)),
            new Station("DBC", "DBC", "DBK", @"Дубки", new Coordinate(43.0216, 46.841), Color.FromArgb(255, 167, 0)),
            new Station("DLMR", "DLM", "DLMR", @"Дылым", new Coordinate(43.0730, 46.6187), Color.FromArgb(255,107,80)),
            new Station("DRN", "DRN", "DRN", @"Дербент", new Coordinate(41.9976, 48.3322), Color.FromArgb(230, 115, 0)),
            new Station("GNBR", "GNB", "GNBR", @"Гуниб", new Coordinate(42.3893, 46.9638), Color.FromArgb(75, 0, 130)),

            new Station("GROC", "GRO", "GROC", @"Грозный", new Coordinate(43.203, 45.796), Color.FromArgb(144,238,144)),
            new Station("KANR", "KRM", "KANR", @"Караман", new Coordinate(43.196, 47.489), Color.FromArgb(0, 255, 127)),
            new Station("KRNR", "KRN", "KRNR", @"Каранай", new Coordinate(42.8267, 46.9053), Color.FromArgb(50,205,50)),
            new Station("KSMR", "KSM", "KSMR", @"Касумкент", new Coordinate(41.6023, 48.1246), Color.FromArgb(32,178,170)),
            new Station("KMKR", "KUM", "KMKR", @"Кумух", new Coordinate(42.1287, 47.0977), Color.FromArgb(107,142,35)),

            new Station("MAK", "MAK", "MAK", @"Махачкала", new Coordinate(42.948, 47.5), Color.FromArgb(135, 206, 250)),
            new Station("SGKR", "SGK", "SGKR", @"Сергокала", new Coordinate(42.4576, 47.655), Color.FromArgb(0, 191, 255)),
            new Station("TRKR", "TRK", "TRKR", @"Терская", new Coordinate(43.723, 44.732), Color.FromArgb(30, 144, 255)),
            new Station("UNCR", "UNC", "UNCR", @"Унцукуль", new Coordinate(42.7155, 46.7929), Color.FromArgb(0, 0, 255)),
            new Station("URKR", "URK", "URKR", @"Уркарах", new Coordinate(42.1645, 47.6316), Color.FromArgb(0, 0, 139)),
            
            new Station("DVE", "VDN", "DVE", @"Ведено", new Coordinate(42.957, 46.126), Color.FromArgb(123, 104, 238)),

            new Station("XNZR", "XNZ", "HNZR", @"Хунзах", new Coordinate(42.5451, 46.7053), Color.FromArgb(138, 43, 226)),

            new Station("TLTR", "TLTR", "TLTR", @"Тлярата", new Coordinate(42.10586, 46.35440), Color.FromArgb(100, 84, 82)),
        };

        /// <summary>
        /// Возвращает первую станцию совпавшую по имени. Если таковой нет, вовзращает null
        /// </summary>
        /// <param name="name">Имя станции</param>
        /// <returns>Станция</returns>
        public static Station GetStation(string name)
        {
            if (name == null)
                return null;
            if (name == "HNZ")
                return Stations.FirstOrDefault(s => s.Name == "XNZR");
            return Stations.FirstOrDefault(s => s.Name == name || s.InternationalName == name || s.NameInDb == name);
        }

        /// <summary>
        /// Имя станции (учитывает R)
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Международное имя станции (без R)
        /// </summary>
        public string InternationalName { get; }

        /// <summary>
        /// Имя, которое хранится в БД
        /// </summary>
        public string NameInDb { get; }

        /// <summary>
        /// Русское имя станции
        /// </summary>
        public string RusName { get; }

        /// <summary>
        /// Координаты станции
        /// </summary>
        public Coordinate Coordinate { get; }

        /// <summary>
        /// Координаты станции для GMap
        /// </summary>
        public PointLatLng GMapCoordinate { get; }

        /// <summary>
        /// Цвет станции (для обозначения на карте)
        /// </summary>
        public Color Color { get; }

        /// <summary>
        /// Инициализирует новый объект класса <see cref="Stations"/>
        /// </summary>
        /// <param name="name">Имя</param>
        /// <param name="internationalName">Интернациональное имя</param>
        /// <param name="nameInDb">Имя для БД</param>
        /// <param name="rusName">Российское имя</param>
        /// <param name="coordinate">Координаты</param>
        /// <param name="color">Цвет</param>
        private Station(string name, string internationalName, string nameInDb, string rusName, Coordinate coordinate, Color color)
        {
            Name = name;
            InternationalName = internationalName;
            NameInDb = nameInDb;
            RusName = rusName;
            Coordinate = coordinate;
            GMapCoordinate = new PointLatLng(coordinate.Lat, coordinate.Lng);
            Color = color;
        }

        /// <summary>
        /// Возвращает копию класса
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return MemberwiseClone();
        }

        /// <summary>
        /// Возвращает копию класса
        /// </summary>
        /// <returns></returns>
        public Station Copy()
        {
            return (Station) MemberwiseClone();
        }
    }
}
