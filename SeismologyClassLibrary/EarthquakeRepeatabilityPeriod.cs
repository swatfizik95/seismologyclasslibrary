﻿using System.Collections.Generic;

namespace SeismologyClassLibrary
{
    public class EarthquakeRepeatabilityPeriod
    {
        public Period Period { get; set; }
        public List<EarthquakeRepeatability> Repeatabilities { get; set; } = new List<EarthquakeRepeatability>();
    }
}