﻿using System;
using System.Collections.Generic;
using SeismologyClassLibrary.Map;

namespace SeismologyClassLibrary.MathS
{
    /// <summary>
    /// Предоставляет метод вычисления гипоцентра
    /// </summary>
    public interface IHypocentreMethod
    {
        /// <summary>
        /// Имя метода
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Объект интерфейса <see cref="IHypocentreErrorMethod"/>, который показывает каким бразом будет рассчитана погрешность
        /// </summary>
        IHypocentreErrorMethod HypocentreErrorMethod { get; set; }

        /// <summary>
        /// Вычисляет гипоцентр на основании станционных данных и времени t0
        /// </summary>
        /// <param name="stations">Станционные данные</param>
        /// <param name="t0">Время t0</param>
        /// <param name="minDepth">Глбуина, с которой пойдет отсчет</param>
        /// <param name="maxDepth">Шлубина, до которой дойдет отсчет</param>
        /// <param name="depthStep">Шаг глубины</param>
        /// <returns></returns>
        Hypocentre ComputeHypocentre(List<EarthquakeStation> stations, DateTime t0, double minDepth = 3, double maxDepth = 80, double depthStep = 3);
        /// <summary>
        /// Вычисляет эпицентр на основании станционных данных и времени t0 и глубины
        /// </summary>
        /// <param name="stations">Станционные данные</param>
        /// <param name="t0">Время t0</param>
        /// <param name="depth">Глубина</param>
        /// <returns></returns>
        Hypocentre ComputeEpicenter(List<EarthquakeStation> stations, DateTime t0, double depth);
    }
}