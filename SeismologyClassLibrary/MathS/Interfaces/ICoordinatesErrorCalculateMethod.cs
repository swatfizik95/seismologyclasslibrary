﻿using System.Collections.Generic;

namespace SeismologyClassLibrary.MathS
{
    /// <summary>
    /// Предоставляет метод вычисления погрешности координат
    /// </summary>
    public interface ICoordinatesErrorCalculateMethod
    {
        void CalculateCoordinatesError(IEnumerable<EarthquakeStation> stations, double lat, double lng, out double latError, out double lngError);
    }
}
