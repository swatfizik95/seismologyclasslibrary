﻿using System;
using System.Collections.Generic;
using SeismologyClassLibrary.Map;

namespace SeismologyClassLibrary.MathS
{
    /// <summary>
    /// Предоставляет метод вычисления эпицентра
    /// </summary>
    public interface IEpicenterCalculateMethod
    {
        /// <summary>
        /// Имя метода
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Метод, с помощью которого будет вычеслять погрешность при уточнении погрешности
        /// </summary>
        IEpicenterErrorCalculateMethod ErrorCalculateMethod { get; set; }

        /// <summary>
        /// Вычисляет эпицентр на основании станционных данных
        /// </summary>
        /// <param name="stations">Станционные данные</param>
        /// <param name="depth">Глубина</param>
        /// <returns></returns>
        Epicenter Calculate(List<EarthquakeStation> stations, double depth);
    }
}