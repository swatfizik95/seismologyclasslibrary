﻿namespace SeismologyClassLibrary.MathS
{
    /// <summary>
    /// Предоставляет метод вычисления радиуса (эпицентрального расстояния)
    /// </summary>
    public interface IRadiusMethod
    {
        /// <summary>
        /// Вычисляет радиус
        /// </summary>
        /// <param name="value1">Параметр 1</param>
        /// <param name="value2">Параметр 2</param>
        /// <returns></returns>
        double ComputingRadius(double value1, double value2);
    }
}