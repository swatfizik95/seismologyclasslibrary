﻿using System;
using System.Collections.Generic;
using SeismologyClassLibrary.Map;

namespace SeismologyClassLibrary.MathS
{
    /// <summary>
    /// Предоставляет метод вычисления погрешности метода вычисления гипоцентра
    /// </summary>
    public interface IHypocentreErrorMethod
    {
        /// <summary>
        /// Объект интерфейса <see cref="IRadiusMethod"/>, который показывает каким бразом будет рассчитан радиус (эпицентральное расстояние)
        /// </summary>
        IRadiusMethod RadiusMethod { get; set; }
        /// <summary>
        /// Вычисляет погрешность вычисления гипоцентра
        /// </summary>
        /// <param name="stations"></param>
        /// <param name="t0"></param>
        /// <param name="lat"></param>
        /// <param name="lng"></param>
        /// <param name="depth"></param>
        /// <returns></returns>
        double ComputingHypocentreError(IEnumerable<EarthquakeStation> stations, DateTime t0, double lat, double lng, double depth);
        /// <summary>
        /// Вычисляет погрешность вычисления гипоцентра
        /// </summary>
        /// <param name="stations"></param>
        /// <param name="t0"></param>
        /// <param name="hypocentre"></param>
        /// <returns></returns>
        double ComputingHypocentreError(IEnumerable<EarthquakeStation> stations, DateTime t0, Hypocentre hypocentre);
    }
}