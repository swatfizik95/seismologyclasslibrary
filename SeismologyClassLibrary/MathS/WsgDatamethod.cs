﻿using System;
using System.Collections.Generic;
using SeismologyClassLibrary.Map;

namespace SeismologyClassLibrary.MathS
{
    /// <summary>
    /// Предоставляет метод пустышку, который не вычисляет, так как берет данные с WSG файла
    /// </summary>
    public class WsgDataMethod : IEpicenterCalculateMethod
    {
        private IEpicenterErrorCalculateMethod _errorCalculateMethod = new DispersionMethod();

        /// <inheritdoc />
        public string Name { get; } = "Данные из WSG";

        /// <inheritdoc />
        public IEpicenterErrorCalculateMethod ErrorCalculateMethod
        {
            get => _errorCalculateMethod;
            set => _errorCalculateMethod = value ?? throw new ArgumentNullException(nameof(value));
        }
        /// <inheritdoc />
        public Epicenter Calculate(List<EarthquakeStation> stations, double depth)
        {
            return new Epicenter();
        }
    }
}