﻿using System;
using System.Collections.Generic;
using System.Linq;
using SeismologyClassLibrary.Map;

namespace SeismologyClassLibrary.MathS
{
    /// <summary>
    /// Представляет математику для сейсмологии
    /// </summary>
    public class MathS
    {
        #region Old
        /// <summary>
        /// Находит отношение скоростей и время в очаге с помощью МНК по данным прихода T и S волн
        /// </summary>
        /// <param name="tp">Время прихода T волн</param>
        /// <param name="ts">Время прихода S волн</param>
        /// <param name="t0">Найденное время в очаге</param>
        /// <param name="v">Найденное отношение скоростей</param>
        /// <returns></returns>
        public void FindVpVsAndT0(List<DateTime> tp, List<DateTime> ts, out double v, out DateTime t0)
        {
            if (tp.Count != ts.Count)
                throw new Exception("Размеры массивов не совпадают");
            int count = tp.Count;
            var minTp = tp.Min();
            var timeToCompare = new DateTime(
                minTp.Year,
                minTp.Month,
                minTp.Day,
                minTp.Hour, 0, 0, 0);
            var tpArrMs = Enumerable
                .Range(0, count)
                .Select(i => (tp[i] - timeToCompare).TotalMilliseconds)
                .ToArray();
            var tsArrMs = Enumerable
                .Range(0, count)
                .Select(i => (ts[i] - timeToCompare).TotalMilliseconds)
                .ToArray();
            var spArrMs = Enumerable
                .Range(0, count)
                .Select(i => tsArrMs[i] - tpArrMs[i])
                .ToArray();
            double[] coeffP = OrdinaryLeastSquares(tpArrMs, spArrMs);
            double[] coeffS = OrdinaryLeastSquares(tsArrMs, spArrMs);
            double t0Pms = -1 * (coeffP[1] / coeffP[0]);
            double t0Sms = -1 * (coeffS[1] / coeffS[0]);
            double t0Ms = (t0Pms + t0Sms) / 2.0;

            t0 = timeToCompare.AddMilliseconds(t0Ms);

            double speed = 0.0;
            for (int i = 0; i < count; i++)
            {
                speed += 1 + spArrMs[i] / (tpArrMs[i] - t0Ms);
            }
            v = speed / count;
        }

        /// <summary>
        /// МНК (Метод наименьнших квадратов)
        /// </summary>
        /// <param name="x">Значения X</param>
        /// <param name="y">Значения Y</param>
        /// <returns>Коэффициенты</returns>
        public double[] OrdinaryLeastSquares(double[] x, double[] y)
        {
            if (x.Length != y.Length)
                throw new Exception("Размеры массивов не совпадают");
            double xSum, ySum, x2Sum, xySum;
            xSum = ySum = x2Sum = xySum = 0;
            int n = x.Length;
            for (int i = 0; i < n; i++)
            {
                xSum += x[i];
                ySum += y[i];
                x2Sum += x[i] * x[i];
                xySum += x[i] * y[i];
            }

            double delta = xSum * xSum - n * x2Sum;
            double[] coeff = new double[2];
            coeff[0] = (ySum * xSum - xySum * n) / delta;
            coeff[1] = (xSum * xySum - x2Sum * ySum) / delta;
            return coeff;
        }

        /// <summary>
        /// Находит значения x квадртаного уравнения (2й степени)
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        public double[] SolveQuadraticEquation(double a, double b, double c)
        {
            double d = b * b - 4 * a * c;

            if (d > 0)
            {
                return new[] { (-b + Math.Sqrt(d)) / (2 * a), (-b - Math.Sqrt(d)) / (2 * a) };
            }

            if (d < 0)
            {
                return new[] { double.NaN, double.NaN };
            }

            return new[] { -b / (2 * a), -b / (2 * a) };
        }

        /// <summary>
        /// Находит значения x кубического уравнения (3й степени) по формуле Виета
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="d"></param>
        /// <returns></returns>
        public double[] SolveCubicEquation(double a, double b, double c,
            double d)
        {
            double
                aS = b / a,
                bS = c / a,
                cS = d / a;
            double q = (aS * aS - 3.0 * bS) / 9.0;
            double q3 = q * q * q;
            double r = (2.0 * aS * aS * aS - 9.0 * aS * bS + 27.0 * cS) / 54.0;
            double s = q3 - r * r;
            double[] result = { double.NaN, double.NaN, double.NaN };
            if (s > 0)
            {
                double fi = 1.0 / 3.0 * Math.Acos(r / Math.Sqrt(q3));

                result[0] = -2 * Math.Sqrt(q) * Math.Cos(fi) - aS / 3.0;
                result[1] = -2 * Math.Sqrt(q) * Math.Cos(fi + 2.0 / 3.0 * Math.PI) - aS / 3.0;
                result[2] = -2 * Math.Sqrt(q) * Math.Cos(fi - 2.0 / 3.0 * Math.PI) - aS / 3.0;
            }
            else if (s < 0)
            {
                double Arsh(double x) => Math.Log(x + Math.Sqrt(x * x + 1));
                if (q > 0)
                {
                    double fi = 1.0 / 3.0 * Arch(Math.Abs(r) / Math.Sqrt(q3));
                    result[0] = -2 * Math.Sign(r) * Math.Sqrt(q) * Math.Cosh(fi) - aS / 3.0;
                }
                else if (q < 0)
                {
                    double fi = 1.0 / 3.0 * Arsh(Math.Abs(r) / Math.Sqrt(Math.Abs(q3)));
                    result[0] = -2 * Math.Sign(r) * Math.Sqrt(Math.Abs(q)) * Math.Sinh(fi) - aS / 3.0;
                }
            }

            return result;
        }

        /// <summary>
        /// Находит значения x уравнения 4й степени
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="d"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        public double[] SolveEquationFourthDegree(double a, double b, double c, double d, double e)
        {
            double
                aS = b / a,
                bS = c / a,
                cS = d / a,
                dS = e / a;
            // Переводим в куб уравнение
            double
                a3 = 1.0,
                b3 = -bS,
                c3 = cS * aS - 4.0 * dS,
                d3 = -(cS * cS + dS * aS * aS - 4.0 * dS * bS);

            // Находим куб решение
            var res3 = SolveCubicEquation(a3, b3, c3, d3);

            // Выбираем ответы только больше нуля
            var results = new List<double>();
            if (res3[0] > 0)
                results.Add(res3[0]);
            if (res3[1] > 0)
                results.Add(res3[1]);
            if (res3[2] > 0)
                results.Add(res3[2]);

            double u1 = double.NaN;

            if (results.Count > 1)
            {
                foreach (var result in results)
                {
                    if (aS * aS / 4.0 + u1 - bS > 0)
                    {
                        u1 = result;
                        break;
                    }
                }
            }
            else if (results.Count == 1)
            {
                u1 = results[0];
            }

            double p1 = aS / 2.0 + Math.Sqrt(aS * aS / 4.0 + u1 - bS);
            double p2 = aS / 2.0 - Math.Sqrt(aS * aS / 4.0 + u1 - bS);
            double q1 = u1 / 2.0 + Math.Sqrt(u1 * u1 / 4 - dS);
            double q2 = u1 / 2.0 - Math.Sqrt(u1 * u1 / 4 - dS);

            var res21 = SolveQuadraticEquation(1, p1, q1);
            var res22 = SolveQuadraticEquation(1, p2, q2);

            return new[] { res21[0], res21[1], res22[0], res22[1] };
        }

        /// <summary>
        /// Обратная функция гиперболического косинуса
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public double Arch(double x) => Math.Log(x + Math.Sqrt(x * x - 1));

        /// <summary>
        /// Обратная функция гиперболического синуса
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public double Arsh(double x) => Math.Log(x + Math.Sqrt(x * x + 1));
        #endregion

        /// <summary>
        /// Находит точку пересечения двух эллипсов на Земном Шаре численным методом
        /// </summary>
        /// <param name="c1">Координаты центра 1го эллипса</param>
        /// <param name="r1">Радиус (в км) 1го эллипса</param>
        /// <param name="c2">Координаты центра 2го эллипса</param>
        /// <param name="r2">Радиус (в км) 2го эллипса</param>
        /// <returns></returns>
        public List<Coordinate> FindEllipseIntersectionPoints(Coordinate c1, double r1, Coordinate c2, double r2)
        {
            var result = new List<Coordinate>();

            Coordinate center1;
            Coordinate center2;
            double radius1;
            double radius2;

            if (r1 > r2)
            {
                radius1 = r1;
                center1 = new Coordinate(c1);
                radius2 = r2;
                center2 = new Coordinate(c2);
            }
            else
            {
                radius1 = r2;
                center1 = new Coordinate(c2);
                radius2 = r1;
                center2 = new Coordinate(c1);
            }

            var radius = new Coordinate(Coordinate.KmToLat(radius1), Coordinate.KmToLng(radius1, center1.Lat));

            // Находим точку, в которая будет ближе всего к центру 2го эллипса
            double bestDistanceTo = double.PositiveInfinity;
            var bestPoint = new Coordinate(center1.Lat, center1.Lng);
            double degreeMax = 2 * Math.PI;
            double degreeStep = degreeMax * 5 / 360.0;
            double degreeCenter = 0.0;
            for (double degreeCurrent = 0; degreeCurrent < degreeMax; degreeCurrent += degreeStep)
            {
                var point = new Coordinate(
                    center1.Lat + radius.Lat * Math.Sin(degreeCurrent),
                    center1.Lng + radius.Lng * Math.Cos(degreeCurrent)
                );
                double distanceTo = center2.DistanceTo(point);
                if (distanceTo < bestDistanceTo)
                {
                    bestDistanceTo = distanceTo;
                    bestPoint.Lat = point.Lat;
                    bestPoint.Lng = point.Lng;
                    degreeCenter = degreeCurrent;
                }
            }

            if (radius2 - bestDistanceTo < -2.5)
            {
                return result;
            }

            // Ищем правую точку
            var rightPoint = new Coordinate();
            double bestError = double.PositiveInfinity;
            double degree = degreeCenter;
            degreeStep = degreeMax / 1440.0;
            while (bestError > 0.05)
            {
                var point = new Coordinate(
                    center1.Lat + radius.Lat * Math.Sin(degree),
                    center1.Lng + radius.Lng * Math.Cos(degree)
                );

                double error = Math.Abs(radius2 - center2.DistanceTo(point));
                if (error < bestError)
                {
                    bestError = error;
                    rightPoint.Lat = point.Lat;
                    rightPoint.Lng = point.Lng;
                }

                degree += degreeStep;
                if (degree > degreeCenter + Math.PI)
                {
                    break;
                }
            }
            result.Add(rightPoint);
            // Ищем левую точку
            var leftPoint = new Coordinate();
            bestError = double.PositiveInfinity;
            degree = degreeCenter;
            while (bestError > 0.05)
            {
                var point = new Coordinate(
                    center1.Lat + radius.Lat * Math.Sin(degree),
                    center1.Lng + radius.Lng * Math.Cos(degree)
                );
                double error = Math.Abs(radius2 - center2.DistanceTo(point));
                if (error < bestError)
                {
                    bestError = error;
                    leftPoint.Lat = point.Lat;
                    leftPoint.Lng = point.Lng;
                }

                degree -= degreeStep;
                if (degree < degreeCenter - Math.PI)
                {
                    break;
                }
            }
            result.Add(leftPoint);

            return result;
        }
    }
}
