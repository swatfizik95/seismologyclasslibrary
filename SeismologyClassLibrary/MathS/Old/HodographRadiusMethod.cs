﻿using System;

namespace SeismologyClassLibrary.MathS
{
    public class HodographRadiusMethod : IRadiusMethod
    {
        public double ComputingRadius(double h, double tpt0)
        {
            if (h < 4.75)
            {
                return 0.02431567 * Math.Pow(tpt0, 3.0) - 0.4171853 * Math.Pow(tpt0, 2.0) + 6.488 * tpt0 - 4.98;
            }

            if (h < 7.5)
            {
                return 0.00083306 * Math.Pow(tpt0, 3.0) - 0.0264100 * Math.Pow(tpt0, 2.0) + 5.173 * tpt0 - 4.84;
            }

            if (h < 11.0)
            {
                return 0.00728480 * Math.Pow(tpt0, 3.0) - 0.1939342 * Math.Pow(tpt0, 2.0) + 6.857 * tpt0 - 9.94;
            }

            if (h < 15.0)
            {
                return 0.00432369 * Math.Pow(tpt0, 3.0) - 0.1859166 * Math.Pow(tpt0, 2.0) + 7.947 * tpt0 - 16.4;
            }

            if (h < 20.0)
            {
                return 0.02401886 * Math.Pow(tpt0, 3.0) - 0.7260308 * Math.Pow(tpt0, 2.0) + 12.8834 * tpt0 - 31.5;
            }

            if (h < 25.0)
            {
                return 0.01223406 * Math.Pow(tpt0, 3.0) - 0.5052138 * Math.Pow(tpt0, 2.0) + 12.308 * tpt0 - 37.1;
            }

            if (h < 30.0)
            {
                return 0.02795078 * Math.Pow(tpt0, 3.0) - 1.118505 * Math.Pow(tpt0, 2.0) + 20.118 * tpt0 - 63.7;
            }

            if (h < 35.0)
            {
                return 0.01180902 * Math.Pow(tpt0, 3.0) - 0.5687039 * Math.Pow(tpt0, 2.0) + 15.118 * tpt0 - 61.5;
            }

            if (h < 40)
            {
                return 0.02793680 * Math.Pow(tpt0, 3.0) - 1.238933 * Math.Pow(tpt0, 2.0) + 25.127 * tpt0 - 116.6;
            }

            if (h < 45)
            {
                return 0.02326818 * Math.Pow(tpt0, 3.0) - 1.192842 * Math.Pow(tpt0, 2.0) + 28.116 * tpt0 - 170.0;
            }

            if (h < 50)
            {
                return 0.008126314 * Math.Pow(tpt0, 3.0) - 0.4635466 * Math.Pow(tpt0, 2.0) + 15.225 * tpt0 - 97.8;
            }

            if (h < 55)
            {
                return 0.005379117 * Math.Pow(tpt0, 3.0) - 0.4227418 * Math.Pow(tpt0, 2.0) + 15.050 * tpt0 - 79.78;
            }

            if (h < 60)
            {
                return 0.007647251 * Math.Pow(tpt0, 3.0) - 0.5752625 * Math.Pow(tpt0, 2.0) + 19.939 * tpt0 - 110.7;
            }

            if (h < 65)
            {
                return 0.002470946 * Math.Pow(tpt0, 3.0) - 0.230207 * Math.Pow(tpt0, 2.0) + 10.235 * tpt0 - 64.8;
            }

            if (h < 70)
            {
                return 0.02423239 * Math.Pow(tpt0, 3.0) - 1.305333 * Math.Pow(tpt0, 2.0) + 28.769 * tpt0 - 168.7;
            }

            if (h < 75)
            {
                return 0.02200796 * Math.Pow(tpt0, 3.0) - 1.458171 * Math.Pow(tpt0, 2.0) + 38.058 * tpt0 - 224.9;
            }

            return 0.04246612 * Math.Pow(tpt0, 3.0) - 2.764635 * Math.Pow(tpt0, 2.0) + 66.513 * tpt0 - 476.6;
        }
    }
}