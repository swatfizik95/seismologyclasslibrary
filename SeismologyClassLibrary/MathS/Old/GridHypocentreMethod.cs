﻿using System;
using System.Collections.Generic;
using System.Linq;
using SeismologyClassLibrary.Map;

namespace SeismologyClassLibrary.MathS
{
    public class GridHypocentreMethod : IHypocentreMethod
    {
        public string Name { get; } = "Метод Сеток";
        private IHypocentreErrorMethod _hypocentreErrorMethod;
        public IHypocentreErrorMethod HypocentreErrorMethod
        {
            get => _hypocentreErrorMethod;
            set => _hypocentreErrorMethod = value ?? throw new ArgumentNullException(nameof(value));
        }

        public int Count { get; set; } = 100;
        public int IterationCount { get; set; } = 2;

        private double GetRadius(double h, double tpt0)
        {
            return _hypocentreErrorMethod.RadiusMethod.ComputingRadius(h, tpt0);
        }

        public Hypocentre ComputeHypocentre(List<EarthquakeStation> stations, DateTime t0, double minDepth = 3, double maxDepth = 80, double depthStep = 3)
        {
            var hypocentre = new Hypocentre();
            var error = double.PositiveInfinity;

            Coordinate
                startCoord = new Coordinate(),
                endCoord = new Coordinate(),
                stepCoord = new Coordinate();

            var coorrectStations = stations.Where(s => s?.Station != null).ToList();

            for (double depth = minDepth; depth < maxDepth; depth += depthStep)
            {
                // Проверка, если при такой глубине хоть один радиус по станции будет < 0, то прекращаем
                if (coorrectStations.Any(s => GetRadius(depth, (s.Tp - t0).TotalSeconds) < 0))
                {
                    break;
                }

                startCoord.Lat = coorrectStations.Select(s =>
                    s.Station.Coordinate.Lat - Coordinate.KmToLat(GetRadius(depth, (s.Tp - t0).TotalSeconds))).Min();
                startCoord.Lng = coorrectStations.Select(s =>
                    s.Station.Coordinate.Lng - Coordinate.KmToLng(GetRadius(depth, (s.Tp - t0).TotalSeconds), s.Station.Coordinate.Lat)).Min();

                endCoord.Lat = coorrectStations.Select(s =>
                    s.Station.Coordinate.Lat + Coordinate.KmToLat(GetRadius(depth, (s.Tp - t0).TotalSeconds))).Max();
                endCoord.Lng = coorrectStations.Select(s =>
                    s.Station.Coordinate.Lng + Coordinate.KmToLng(GetRadius(depth, (s.Tp - t0).TotalSeconds), s.Station.Coordinate.Lat)).Max();

                stepCoord.Lat = (endCoord.Lat - startCoord.Lat) / Count;
                stepCoord.Lng = (endCoord.Lng - startCoord.Lng) / Count;

                double minErrorOnIteration = double.PositiveInfinity;
                var coord = new Coordinate();

                for (int iteration = 0; iteration < IterationCount; iteration++)
                {
                    for (int i = 0; i < Count; i++)
                    {
                        for (int j = 0; j < Count; j++)
                        {
                            double currentError = _hypocentreErrorMethod.ComputingHypocentreError(
                                coorrectStations,
                                t0,
                                startCoord.Lat + i * stepCoord.Lat + stepCoord.Lat * 0.5,
                                startCoord.Lng + j * stepCoord.Lng + stepCoord.Lng * 0.5,
                                depth);

                            if (minErrorOnIteration > currentError)
                            {
                                coord.Lat = startCoord.Lat + i * stepCoord.Lat + stepCoord.Lat * 0.5;
                                coord.Lng = startCoord.Lng + j * stepCoord.Lng + stepCoord.Lng * 0.5;

                                minErrorOnIteration = currentError;
                            }
                        }
                    }

                    startCoord.Lat = coord.Lat - stepCoord.Lat * 0.5;
                    startCoord.Lng = coord.Lng - stepCoord.Lng * 0.5;

                    endCoord.Lat = coord.Lat + stepCoord.Lat * 0.5;
                    endCoord.Lng = coord.Lng + stepCoord.Lng * 0.5;

                    stepCoord.Lat = (endCoord.Lat - startCoord.Lat) / Count;
                    stepCoord.Lng = (endCoord.Lng - startCoord.Lng) / Count;
                }

                if (error > minErrorOnIteration)
                {
                    if (error / minErrorOnIteration > 1.25)
                    {
                        hypocentre.Lat = coord.Lat;
                        hypocentre.Lng = coord.Lng;
                        hypocentre.Depth = depth;

                        error = minErrorOnIteration;
                    }
                }
            }

            return hypocentre;
        }

        public Hypocentre ComputeEpicenter(List<EarthquakeStation> stations, DateTime t0, double depth)
        {
            Coordinate
                startCoord = new Coordinate(),
                endCoord = new Coordinate(),
                stepCoord = new Coordinate();

            var stantions = stations.Where(s => s?.Station != null).ToList();

            // Проверка, если при такой глубине хоть один радиус по станции будет < 0, то прекращаем
            if (stantions.Any(s => GetRadius(depth, (s.Tp - t0).TotalSeconds) < 0))
            {
                throw new Exception();
            }

            startCoord.Lat = stantions.Select(s =>
                s.Station.Coordinate.Lat - Coordinate.KmToLat(GetRadius(depth, (s.Tp - t0).TotalSeconds))).Min();
            startCoord.Lng = stantions.Select(s =>
                s.Station.Coordinate.Lng - Coordinate.KmToLng(GetRadius(depth, (s.Tp - t0).TotalSeconds), s.Station.Coordinate.Lat)).Min();

            endCoord.Lat = stantions.Select(s =>
                s.Station.Coordinate.Lat + Coordinate.KmToLat(GetRadius(depth, (s.Tp - t0).TotalSeconds))).Max();
            endCoord.Lng = stantions.Select(s =>
                s.Station.Coordinate.Lng + Coordinate.KmToLng(GetRadius(depth, (s.Tp - t0).TotalSeconds), s.Station.Coordinate.Lat)).Max();

            stepCoord.Lat = (endCoord.Lat - startCoord.Lat) / Count;
            stepCoord.Lng = (endCoord.Lng - startCoord.Lng) / Count;

            double minErrorOnIteration = double.PositiveInfinity;
            var coord = new Coordinate();

            for (int iteration = 0; iteration < IterationCount; iteration++)
            {
                for (int i = 0; i < Count; i++)
                {
                    for (int j = 0; j < Count; j++)
                    {
                        double currentError = _hypocentreErrorMethod.ComputingHypocentreError(
                            stations,
                            t0,
                            new Hypocentre
                            {
                                Lat = startCoord.Lat + i * stepCoord.Lat + stepCoord.Lat * 0.5,
                                Lng = startCoord.Lng + j * stepCoord.Lng + stepCoord.Lng * 0.5,
                                Depth = depth
                            }
                            );

                        if (minErrorOnIteration > currentError)
                        {
                            coord.Lat = startCoord.Lat + i * stepCoord.Lat + stepCoord.Lat * 0.5;
                            coord.Lng = startCoord.Lng + j * stepCoord.Lng + stepCoord.Lng * 0.5;

                            minErrorOnIteration = currentError;
                        }
                    }
                }

                startCoord.Lat = coord.Lat - stepCoord.Lat * 0.5;
                startCoord.Lng = coord.Lng - stepCoord.Lng * 0.5;

                endCoord.Lat = coord.Lat + stepCoord.Lat * 0.5;
                endCoord.Lng = coord.Lng + stepCoord.Lng * 0.5;

                stepCoord.Lat = (endCoord.Lat - startCoord.Lat) / Count;
                stepCoord.Lng = (endCoord.Lng - startCoord.Lng) / Count;
            }

            return new Hypocentre
            {
                Lat = coord.Lat,
                Lng = coord.Lng,
                Depth = depth
            };
        }

        public GridHypocentreMethod()
        {
            _hypocentreErrorMethod = new DispersionErrorMethod();
        }
    }
}
