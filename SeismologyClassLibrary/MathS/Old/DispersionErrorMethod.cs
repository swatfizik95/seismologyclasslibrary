﻿using System;
using System.Collections.Generic;
using System.Linq;
using SeismologyClassLibrary.Map;

namespace SeismologyClassLibrary.MathS
{
    public class DispersionErrorMethod : IHypocentreErrorMethod
    {
        private IRadiusMethod _radiusMethod;
        public IRadiusMethod RadiusMethod
        {
            get => _radiusMethod;
            set => _radiusMethod = value ?? throw new ArgumentNullException(nameof(value));
        }

        public double ComputingHypocentreError(IEnumerable<EarthquakeStation> stations, DateTime t0, double lat, double lng, double depth)
        {
            var stantions = stations.Where(s => s.Station != null);

            var values = stantions.Select(s => Math.Abs(_radiusMethod.ComputingRadius(depth, (s.Tp - t0).TotalSeconds) - s.Station.Coordinate.DistanceTo(lat, lng))).ToList();
            double mat = Math.Pow(values.Average(), 2);

            return values.Select(val => Math.Pow(val, 2) - mat).Average();
        }

        public double ComputingHypocentreError(IEnumerable<EarthquakeStation> stations, DateTime t0, Hypocentre hypocentre)
        {
            var stantions = stations.Where(s => s.Station != null);

            var values = stantions.Select(s => Math.Abs(_radiusMethod.ComputingRadius(hypocentre.Depth, (s.Tp - t0).TotalSeconds) - s.Station.Coordinate.DistanceTo(hypocentre.Lat, hypocentre.Lng))).ToList();
            double mat = Math.Pow(values.Average(), 2);

            return values.Select(val => Math.Pow(val, 2) - mat).Average();
        }

        public DispersionErrorMethod()
        {
            _radiusMethod = new HodographRadiusMethod();
        }
    }
}