﻿using System;
using System.Collections.Generic;
using SeismologyClassLibrary.Map;

namespace SeismologyClassLibrary.MathS
{
    public class AutomaticHypocentreMethod : IHypocentreMethod
    {
        public string Name { get; private set; } = "Автоматический";
        private IHypocentreErrorMethod _hypocentreErrorMethod;
        public IHypocentreErrorMethod HypocentreErrorMethod
        {
            get => _hypocentreErrorMethod;
            set
            {
                _hypocentreErrorMethod = value ?? throw new ArgumentNullException(nameof(value));
                foreach (var method in _hypocentreMethods)
                {
                    method.HypocentreErrorMethod = new DispersionErrorMethod();
                }
            }
        }

        private readonly List<IHypocentreMethod> _hypocentreMethods = new List<IHypocentreMethod>
        {
            new GridHypocentreMethod(),
            new MultiHypocentreMethod(),
            new AverageHypocentreMethod(),
            new BinaryHypocentreMethod()
        };

        public Hypocentre ComputeHypocentre(List<EarthquakeStation> stations, DateTime t0, double minDepth = 3, double maxDepth = 80,
            double depthStep = 3)
        {
            
            var bestHypocentre = new Hypocentre();
            double bestError = double.PositiveInfinity;

            foreach (var method in _hypocentreMethods)
            {
                var hypocentre = method.ComputeHypocentre(stations, t0, minDepth, maxDepth, depthStep);
                double error = _hypocentreErrorMethod.ComputingHypocentreError(stations, t0, hypocentre);
                if (bestError > error)
                {
                    bestError = error;
                    bestHypocentre = hypocentre;
                    Name = method.Name;
                }
            }

            return bestHypocentre;
        }

        public Hypocentre ComputeEpicenter(List<EarthquakeStation> stations, DateTime t0, double depth)
        {
            var bestHypocentre = new Hypocentre();
            double bestError = double.PositiveInfinity;

            foreach (var method in _hypocentreMethods)
            {
                var hypocentre = method.ComputeEpicenter(stations, t0, depth);
                double error = _hypocentreErrorMethod.ComputingHypocentreError(stations, t0, hypocentre);
                if (bestError > error)
                {
                    bestError = error;
                    bestHypocentre = hypocentre;
                    Name = method.Name;
                }
            }

            return bestHypocentre;
        }

        public AutomaticHypocentreMethod()
        {
            _hypocentreErrorMethod = new DispersionErrorMethod();
            foreach (var method in _hypocentreMethods)
            {
                method.HypocentreErrorMethod = new DispersionErrorMethod();
            }
        }
    }
}