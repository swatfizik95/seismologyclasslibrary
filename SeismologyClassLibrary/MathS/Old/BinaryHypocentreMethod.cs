﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using SeismologyClassLibrary.Map;

namespace SeismologyClassLibrary.MathS
{
    public class BinaryHypocentreMethod : IHypocentreMethod
    {
        public string Name { get; } = "Двоичный Метод";
        private IHypocentreErrorMethod _hypocentreErrorMethod;
        public IHypocentreErrorMethod HypocentreErrorMethod
        {
            get => _hypocentreErrorMethod;
            set => _hypocentreErrorMethod = value ?? throw new ArgumentNullException(nameof(value));
        }

        private double GetRadius(double h, double tpt0)
        {
            return _hypocentreErrorMethod.RadiusMethod.ComputingRadius(h, tpt0);
        }

        public Hypocentre ComputeHypocentre(List<EarthquakeStation> stations, DateTime t0, double minDepth = 3, double maxDepth = 80,
            double depthStep = 3)
        {
            var hypocentre = new Hypocentre();
            double error = double.PositiveInfinity;

            Coordinate
                startCoord = new Coordinate(),
                endCoord = new Coordinate(),
                stepCoord = new Coordinate();

            var correctStations = stations.Where(s => s?.Station != null).ToList();

            for (double depth = minDepth; depth < maxDepth; depth += depthStep)
            {
                // Проверка, если при такой глубине хоть один радиус по станции будет < 0, то прекращаем
                if (correctStations.Any(s => GetRadius(depth, (s.Tp - t0).TotalSeconds) < 0))
                {
                    break;
                }

                startCoord.Lat = correctStations.Select(s =>
                    s.Station.Coordinate.Lat - Coordinate.KmToLat(GetRadius(depth, (s.Tp - t0).TotalSeconds))).Min();
                startCoord.Lng = correctStations.Select(s =>
                    s.Station.Coordinate.Lng - Coordinate.KmToLng(GetRadius(depth, (s.Tp - t0).TotalSeconds), s.Station.Coordinate.Lat)).Min();

                endCoord.Lat = correctStations.Select(s =>
                    s.Station.Coordinate.Lat + Coordinate.KmToLat(GetRadius(depth, (s.Tp - t0).TotalSeconds))).Max();
                endCoord.Lng = correctStations.Select(s =>
                    s.Station.Coordinate.Lng + Coordinate.KmToLng(GetRadius(depth, (s.Tp - t0).TotalSeconds), s.Station.Coordinate.Lat)).Max();

                stepCoord.Lat = (endCoord.Lat - startCoord.Lat) / 2.0;
                stepCoord.Lng = (endCoord.Lng - startCoord.Lng) / 2.0;

                double minErrorOnStep = double.PositiveInfinity;
                for (int step = 0; step < 100; step++)
                {
                    // 1) Смотрим слева сверху '+
                    var coord = new Coordinate
                    {
                        Lng = 0,
                        Lat = 0
                    };
                    var updEndCoord = new Coordinate
                    {
                        Lat = 0,
                        Lng = 0
                    };
                    var updStartCoord = new Coordinate
                    {
                        Lat = 0,
                        Lng = 0
                    };
                    coord.Lat = endCoord.Lat - stepCoord.Lat / 2.0;
                    coord.Lng = startCoord.Lng + stepCoord.Lng / 2.0;
                    double errorOnStep = _hypocentreErrorMethod.ComputingHypocentreError(stations, t0, coord.Lat, coord.Lng, depth);

                    // Берем значения по дефолту
                    updStartCoord.Lat = endCoord.Lat - stepCoord.Lat;
                    updEndCoord.Lat = endCoord.Lat;
                    updStartCoord.Lng = startCoord.Lng;
                    updEndCoord.Lng = startCoord.Lng + stepCoord.Lng;

                    // 2) Смотрим справа сверху +'
                    coord.Lat = endCoord.Lat - stepCoord.Lat / 2.0;
                    coord.Lng = endCoord.Lng - stepCoord.Lng / 2.0;
                    double newError = _hypocentreErrorMethod.ComputingHypocentreError(stations, t0, coord.Lat, coord.Lng, depth);
                    // Если ошибка на новом положении меньше, то меняем значения
                    if (newError < errorOnStep)
                    {
                        errorOnStep = newError;
                        updStartCoord.Lat = endCoord.Lat - stepCoord.Lat;
                        updEndCoord.Lat = endCoord.Lat;
                        updStartCoord.Lng = endCoord.Lng - stepCoord.Lng;
                        updEndCoord.Lng = endCoord.Lng;
                    }

                    // 3) Смотрим слева снизу .+
                    coord.Lat = startCoord.Lat + stepCoord.Lat / 2.0;
                    coord.Lng = startCoord.Lng + stepCoord.Lng / 2.0;
                    newError = _hypocentreErrorMethod.ComputingHypocentreError(stations, t0, coord.Lat, coord.Lng, depth);
                    if (newError < errorOnStep)
                    {
                        errorOnStep = newError;
                        updStartCoord.Lat = startCoord.Lat;
                        updEndCoord.Lat = startCoord.Lat + stepCoord.Lat;
                        updStartCoord.Lng = startCoord.Lng;
                        updEndCoord.Lng = startCoord.Lng + stepCoord.Lng;
                    }

                    // 4) Смотрим справа снизу +.
                    coord.Lat = startCoord.Lat + stepCoord.Lat / 2.0;
                    coord.Lng = endCoord.Lng - stepCoord.Lng / 2.0;
                    newError = _hypocentreErrorMethod.ComputingHypocentreError(stations, t0, coord.Lat, coord.Lng, depth);
                    if (newError < errorOnStep)
                    {
                        errorOnStep = newError;
                        updStartCoord.Lat = startCoord.Lat;
                        updEndCoord.Lat = startCoord.Lat + stepCoord.Lat;
                        updStartCoord.Lng = endCoord.Lng - stepCoord.Lng;
                        updEndCoord.Lng = endCoord.Lng;
                    }

                    // Если ошибка на прошлом шаге больше, чем на нынешнем, то
                    if (minErrorOnStep > errorOnStep)
                    {
                        endCoord.Lat = updEndCoord.Lat;
                        startCoord.Lat = updStartCoord.Lat;
                        startCoord.Lng = updStartCoord.Lng;
                        endCoord.Lng = updEndCoord.Lng;

                        stepCoord.Lat = (endCoord.Lat - startCoord.Lat) / 2.0;
                        stepCoord.Lng = (endCoord.Lng - startCoord.Lng) / 2.0;

                        minErrorOnStep = errorOnStep;
                    }
                    // Иначе сужаем область поиска
                    else
                    {
                        endCoord.Lat = endCoord.Lat - stepCoord.Lat / 2.0;
                        startCoord.Lat = startCoord.Lat + stepCoord.Lat / 2.0;
                        startCoord.Lng = startCoord.Lng + stepCoord.Lng / 2.0;
                        endCoord.Lng = endCoord.Lng - stepCoord.Lng / 2.0;

                        stepCoord.Lat = (endCoord.Lat - startCoord.Lat) / 2.0;
                        stepCoord.Lng = (endCoord.Lng - startCoord.Lng) / 2.0;
                    }
                }

                // Если ошибка на этой глубине меньше, то обновляем значения
                if (error > minErrorOnStep)
                {
                    if (error / minErrorOnStep > 1.25)
                    {
                        error = minErrorOnStep;

                        hypocentre.Lat = (endCoord.Lat + startCoord.Lat) / 2.0;
                        hypocentre.Lng = (endCoord.Lng + startCoord.Lng) / 2.0;
                        hypocentre.Depth = depth;
                    }
                }
            }

            return hypocentre;
        }

        public Hypocentre ComputeEpicenter(List<EarthquakeStation> stations, DateTime t0, double depth)
        {
            

            Coordinate
                startCoord = new Coordinate(),
                endCoord = new Coordinate(),
                stepCoord = new Coordinate();

            var correctStations = stations.Where(s => s?.Station != null).ToList();

            // Проверка, если при такой глубине хоть один радиус по станции будет < 0, то прекращаем
            if (correctStations.Any(s => GetRadius(depth, (s.Tp - t0).TotalSeconds) < 0))
            {
                throw new Exception();
            }

            startCoord.Lat = correctStations.Select(s =>
                s.Station.Coordinate.Lat - Coordinate.KmToLat(GetRadius(depth, (s.Tp - t0).TotalSeconds))).Min();
            startCoord.Lng = correctStations.Select(s =>
                s.Station.Coordinate.Lng - Coordinate.KmToLng(GetRadius(depth, (s.Tp - t0).TotalSeconds), s.Station.Coordinate.Lat)).Min();

            endCoord.Lat = correctStations.Select(s =>
                s.Station.Coordinate.Lat + Coordinate.KmToLat(GetRadius(depth, (s.Tp - t0).TotalSeconds))).Max();
            endCoord.Lng = correctStations.Select(s =>
                s.Station.Coordinate.Lng + Coordinate.KmToLng(GetRadius(depth, (s.Tp - t0).TotalSeconds), s.Station.Coordinate.Lat)).Max();

            stepCoord.Lat = (endCoord.Lat - startCoord.Lat) / 2.0;
            stepCoord.Lng = (endCoord.Lng - startCoord.Lng) / 2.0;

            double minErrorOnStep = double.PositiveInfinity;
            for (int step = 0; step < 100; step++)
            {
                // 1) Смотрим слева сверху '+
                var coord = new Coordinate
                {
                    Lng = 0,
                    Lat = 0
                };
                var updEndCoord = new Coordinate
                {
                    Lat = 0,
                    Lng = 0
                };
                var updStartCoord = new Coordinate
                {
                    Lat = 0,
                    Lng = 0
                };
                coord.Lat = endCoord.Lat - stepCoord.Lat / 2.0;
                coord.Lng = startCoord.Lng + stepCoord.Lng / 2.0;
                double errorOnStep = _hypocentreErrorMethod.ComputingHypocentreError(stations, t0, coord.Lat, coord.Lng, depth);

                // Берем значения по дефолту
                updStartCoord.Lat = endCoord.Lat - stepCoord.Lat;
                updEndCoord.Lat = endCoord.Lat;
                updStartCoord.Lng = startCoord.Lng;
                updEndCoord.Lng = startCoord.Lng + stepCoord.Lng;

                // 2) Смотрим справа сверху +'
                coord.Lat = endCoord.Lat - stepCoord.Lat / 2.0;
                coord.Lng = endCoord.Lng - stepCoord.Lng / 2.0;
                double newError = _hypocentreErrorMethod.ComputingHypocentreError(stations, t0, coord.Lat, coord.Lng, depth);
                // Если ошибка на новом положении меньше, то меняем значения
                if (newError < errorOnStep)
                {
                    errorOnStep = newError;
                    updStartCoord.Lat = endCoord.Lat - stepCoord.Lat;
                    updEndCoord.Lat = endCoord.Lat;
                    updStartCoord.Lng = endCoord.Lng - stepCoord.Lng;
                    updEndCoord.Lng = endCoord.Lng;
                }

                // 3) Смотрим слева снизу .+
                coord.Lat = startCoord.Lat + stepCoord.Lat / 2.0;
                coord.Lng = startCoord.Lng + stepCoord.Lng / 2.0;
                newError = _hypocentreErrorMethod.ComputingHypocentreError(stations, t0, coord.Lat, coord.Lng, depth);
                if (newError < errorOnStep)
                {
                    errorOnStep = newError;
                    updStartCoord.Lat = startCoord.Lat;
                    updEndCoord.Lat = startCoord.Lat + stepCoord.Lat;
                    updStartCoord.Lng = startCoord.Lng;
                    updEndCoord.Lng = startCoord.Lng + stepCoord.Lng;
                }

                // 4) Смотрим справа снизу +.
                coord.Lat = startCoord.Lat + stepCoord.Lat / 2.0;
                coord.Lng = endCoord.Lng - stepCoord.Lng / 2.0;
                newError = _hypocentreErrorMethod.ComputingHypocentreError(stations, t0, coord.Lat, coord.Lng, depth);
                if (newError < errorOnStep)
                {
                    errorOnStep = newError;
                    updStartCoord.Lat = startCoord.Lat;
                    updEndCoord.Lat = startCoord.Lat + stepCoord.Lat;
                    updStartCoord.Lng = endCoord.Lng - stepCoord.Lng;
                    updEndCoord.Lng = endCoord.Lng;
                }

                // Если ошибка на прошлом шаге больше, чем на нынешнем, то
                if (minErrorOnStep > errorOnStep)
                {
                    endCoord.Lat = updEndCoord.Lat;
                    startCoord.Lat = updStartCoord.Lat;
                    startCoord.Lng = updStartCoord.Lng;
                    endCoord.Lng = updEndCoord.Lng;

                    stepCoord.Lat = (endCoord.Lat - startCoord.Lat) / 2.0;
                    stepCoord.Lng = (endCoord.Lng - startCoord.Lng) / 2.0;

                    minErrorOnStep = errorOnStep;
                }
                // Иначе сужаем область поиска
                else
                {
                    endCoord.Lat = endCoord.Lat - stepCoord.Lat / 2.0;
                    startCoord.Lat = startCoord.Lat + stepCoord.Lat / 2.0;
                    startCoord.Lng = startCoord.Lng + stepCoord.Lng / 2.0;
                    endCoord.Lng = endCoord.Lng - stepCoord.Lng / 2.0;

                    stepCoord.Lat = (endCoord.Lat - startCoord.Lat) / 2.0;
                    stepCoord.Lng = (endCoord.Lng - startCoord.Lng) / 2.0;
                }
            }

            return new Hypocentre
            {
                Lat = (endCoord.Lat + startCoord.Lat) / 2.0,
                Lng = (endCoord.Lng + startCoord.Lng) / 2.0,
                Depth = depth
            };
        }

        public BinaryHypocentreMethod()
        {
            _hypocentreErrorMethod = new DispersionErrorMethod();
        }
    }
}