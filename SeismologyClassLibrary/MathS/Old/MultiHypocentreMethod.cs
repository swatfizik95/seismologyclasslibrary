﻿using System;
using System.Collections.Generic;
using System.Linq;
using SeismologyClassLibrary.Map;

namespace SeismologyClassLibrary.MathS
{
    public class MultiHypocentreMethod : IHypocentreMethod
    {
        public string Name { get; } = "Мульти Метод";
        private IHypocentreErrorMethod _hypocentreErrorMethod;
        public IHypocentreErrorMethod HypocentreErrorMethod
        {
            get => _hypocentreErrorMethod;
            set => _hypocentreErrorMethod = value ?? throw new ArgumentNullException(nameof(value));
        }

        private double GetRadius(double h, double tpt0)
        {
            return _hypocentreErrorMethod.RadiusMethod.ComputingRadius(h, tpt0);
        }

        public Hypocentre ComputeHypocentre(List<EarthquakeStation> stations, DateTime t0, double minDepth = 3, double maxDepth = 80,
            double depthStep = 3)
        {
            var hypocentre = new Hypocentre();
            /*
             * Статья, по которой происходят вычисления
             * точек пересечения,
             * расположена по адресу
             * http://www.litunovskiy.com/gamedev/intersection_of_two_circles/
             */

            // Невязка для предыдущей гулбины
            var error = double.PositiveInfinity;

            // Список пар точек пересечения для текущей глубины
            var intersections = new List<(Coordinate P3, Coordinate P4)>();

            var correctStations = stations.Where(s => s?.Station != null).ToList();

            // Проходимся по высотам
            for (double depth = minDepth; depth < maxDepth; depth += depthStep)
            {
                // Проверка, если при такой глубине хоть один радиус по станции будет < 0, то прекращаем
                if (correctStations.Any(s => GetRadius(depth, (s.Tp - t0).TotalSeconds) < 0))
                {
                    break;
                }

                intersections.Clear();

                double a, b;
                for (int i = 0; i < correctStations.Count - 1; i++)
                {
                    var p1 = correctStations[i].Station.Coordinate;
                    if (p1 == null) continue;
                    // Получаем delta t для первой станции
                    var tp1 = correctStations[i].Tp - t0;
                    double r1 = GetRadius(depth, tp1.TotalSeconds);
                    for (int j = i + 1; j < correctStations.Count; j++)
                    {
                        var p2 = correctStations[j].Station.Coordinate;
                        if (p2 == null) continue;
                        double dist = p1.DistanceTo(p2);
                        var tp2 = correctStations[j].Tp - t0;
                        double r2 = GetRadius(depth, tp2.TotalSeconds);
                        if (r1 + r2 <= dist || Math.Abs(r1 - r2) > dist) continue;

                        // Следуя нашей статье, находим две точки пересечения окружностей,
                        b = (Math.Pow(r2, 2) - Math.Pow(r1, 2) + Math.Pow(dist, 2)) / (2 * dist);
                        a = dist - b;
                        double hh = Math.Sqrt(Math.Pow(r2, 2) - Math.Pow(b, 2));
                        var p0 = new Coordinate
                        {
                            Lat = p1.Lat + a / dist * (p2.Lat - p1.Lat),
                            Lng = p1.Lng + a / dist * (p2.Lng - p1.Lng)
                        };
                        var p3 = new Coordinate
                        {
                            Lat = p0.Lat - (p2.Lng - p1.Lng) / dist * hh,
                            Lng = p0.Lng + (p2.Lat - p1.Lat) / dist * hh
                        };
                        var p4 = new Coordinate
                        {
                            Lat = p0.Lat + (p2.Lng - p1.Lng) / dist * hh,
                            Lng = p0.Lng - (p2.Lat - p1.Lat) / dist * hh
                        };

                        intersections.Add((p3, p4));
                    }
                }

                /*
                 * Создаем списки пересечений:
                 * Для каждой точки находим среди всех пар точек ближайшие.
                 */
                var pointSets = new List<List<Coordinate>>();
                foreach (var (p13, p14) in intersections)
                {
                    var pointSetP3 = new List<Coordinate>();
                    var pointSetP4 = new List<Coordinate>();
                    foreach (var (p23, p24) in intersections)
                    {
                        a = p13.DistanceTo(p23);
                        b = p13.DistanceTo(p24);
                        pointSetP3.Add(new Coordinate(a < b ? p23 : p24));

                        a = p14.DistanceTo(p23);
                        b = p14.DistanceTo(p24);
                        pointSetP4.Add(new Coordinate(a < b ? p23 : p24));
                    }
                    pointSets.Add(pointSetP3);
                    pointSets.Add(pointSetP4);
                }


                /*
                 * Находим эпицентр по средним значениями intersections
                 * сравниваем с предыдущим значением на невязку
                 * если невязка меньше, сохраняем вместо предыдущего варианта.
                 */
                double bestErrorOnPoints = double.PositiveInfinity;
                var bestHypocentreOnPoints = new Hypocentre();
                foreach (var points in pointSets)
                {
                    var hypocentreOnPoints = new Hypocentre
                    {
                        Lng = points.Average(p => p.Lng),
                        Lat = points.Average(p => p.Lat),
                        Depth = depth
                    };
                    double errorOnPoints = RefinementError(correctStations, t0, ref hypocentreOnPoints);
                    if (errorOnPoints < bestErrorOnPoints)
                    {
                        bestErrorOnPoints = errorOnPoints;

                        bestHypocentreOnPoints.Lat = hypocentreOnPoints.Lat;
                        bestHypocentreOnPoints.Lng = hypocentreOnPoints.Lng;
                        bestHypocentreOnPoints.Depth = hypocentreOnPoints.Depth;
                    }
                }

                if (error > bestErrorOnPoints)
                {
                    if (error / bestErrorOnPoints > 1.25)
                    {
                        error = bestErrorOnPoints;

                        hypocentre.Lat = bestHypocentreOnPoints.Lat;
                        hypocentre.Lng = bestHypocentreOnPoints.Lng;
                        hypocentre.Depth = bestHypocentreOnPoints.Depth;
                    }
                }
            }

            return hypocentre;
        }

        public Hypocentre ComputeEpicenter(List<EarthquakeStation> stations, DateTime t0, double depth)
        {
            // Невязка для предыдущей гулбины
            var error = double.PositiveInfinity;

            var correctStations = stations.Where(s => s?.Station != null).ToList();
            // Проверка, если при такой глубине хоть один радиус по станции будет < 0, то прекращаем
            if (correctStations.Any(s => GetRadius(depth, (s.Tp - t0).TotalSeconds) < 0))
            {
                throw new Exception();
            }

            // Список пар точек пересечения для текущей глубины
            var intersections = new List<(Coordinate P3, Coordinate P4)>();

            double a, b;
            for (int i = 0; i < correctStations.Count - 1; i++)
            {
                var p1 = correctStations[i].Station.Coordinate;
                if (p1 == null) continue;
                // Получаем delta t для первой станции
                var tp1 = correctStations[i].Tp - t0;
                double r1 = GetRadius(depth, tp1.TotalSeconds);
                for (int j = i + 1; j < correctStations.Count; j++)
                {
                    var p2 = correctStations[j].Station.Coordinate;
                    if (p2 == null) continue;
                    double dist = p1.DistanceTo(p2);
                    var tp2 = correctStations[j].Tp - t0;
                    double r2 = GetRadius(depth, tp2.TotalSeconds);
                    if (r1 + r2 <= dist || Math.Abs(r1 - r2) > dist) continue;

                    // Следуя нашей статье, находим две точки пересечения окружностей,
                    b = (Math.Pow(r2, 2) - Math.Pow(r1, 2) + Math.Pow(dist, 2)) / (2 * dist);
                    a = dist - b;
                    double hh = Math.Sqrt(Math.Pow(r2, 2) - Math.Pow(b, 2));
                    var p0 = new Coordinate
                    {
                        Lat = p1.Lat + a / dist * (p2.Lat - p1.Lat),
                        Lng = p1.Lng + a / dist * (p2.Lng - p1.Lng)
                    };
                    var p3 = new Coordinate
                    {
                        Lat = p0.Lat - (p2.Lng - p1.Lng) / dist * hh,
                        Lng = p0.Lng + (p2.Lat - p1.Lat) / dist * hh
                    };
                    var p4 = new Coordinate
                    {
                        Lat = p0.Lat + (p2.Lng - p1.Lng) / dist * hh,
                        Lng = p0.Lng - (p2.Lat - p1.Lat) / dist * hh
                    };

                    intersections.Add((p3, p4));
                }
            }

            /*
             * Создаем списки пересечений:
             * Для каждой точки находим среди всех пар точек ближайшие.
             */
            var pointSets = new List<List<Coordinate>>();
            foreach (var (p13, p14) in intersections)
            {
                var pointSetP3 = new List<Coordinate>();
                var pointSetP4 = new List<Coordinate>();
                foreach (var (p23, p24) in intersections)
                {
                    a = p13.DistanceTo(p23);
                    b = p13.DistanceTo(p24);
                    pointSetP3.Add(new Coordinate(a < b ? p23 : p24));

                    a = p14.DistanceTo(p23);
                    b = p14.DistanceTo(p24);
                    pointSetP4.Add(new Coordinate(a < b ? p23 : p24));
                }
                pointSets.Add(pointSetP3);
                pointSets.Add(pointSetP4);
            }


            var hypocentre = new Hypocentre();
            /*
             * Находим эпицентр по средним значениями intersections
             * сравниваем с предыдущим значением на невязку
             * если невязка меньше, сохраняем вместо предыдущего варианта.
             */
            foreach (var points in pointSets)
            {
                var hypocentreOnPoints = new Hypocentre
                {
                    Lng = points.Average(p => p.Lng),
                    Lat = points.Average(p => p.Lat),
                    Depth = depth
                };
                double errorOnPoints = RefinementError(correctStations, t0, ref hypocentreOnPoints);
                if (errorOnPoints < error)
                {
                    error = errorOnPoints;

                    hypocentre.Lat = hypocentreOnPoints.Lat;
                    hypocentre.Lng = hypocentreOnPoints.Lng;
                    hypocentre.Depth = hypocentreOnPoints.Depth;
                }
            }

            return hypocentre;
        }

        private double RefinementError(List<EarthquakeStation> stations, DateTime t0, ref Hypocentre hypocentre)
        {
            double currentError = _hypocentreErrorMethod.ComputingHypocentreError(stations, t0, hypocentre);
            double minError = currentError;
            double lastError = minError;
            double lng = hypocentre.Lng;
            double lat = hypocentre.Lat;
            double depth = hypocentre.Depth;
            int i1 = 1, j1 = 1;
            double step = 0.5;
            // TODO: Оптимизировать этот кусок кода
            for (int iteration = 0; iteration < 50; iteration++)
            {
                if (Math.Abs(lastError - minError) < 0.01)
                    break;
                lastError = minError;

                for (int i = 0; i <= 2; i++)
                for (int j = 0; j <= 2; j++)
                {
                    currentError = _hypocentreErrorMethod.ComputingHypocentreError(
                        stations,
                        t0,
                        lat + (j - 1.0) * step,
                        lng + (i - 1.0) * step,
                        depth);
                    if (currentError <= minError)
                    {
                        i1 = i; j1 = j;
                        minError = currentError;
                    }
                }
                if (i1 == 1 && j1 == 1)
                {
                    step = step / 2.0;
                }
                else
                {
                    lng = lng + (i1 - 1.0) * step;
                    lat = lat + (j1 - 1.0) * step;
                }
            }

            hypocentre.Lng = lng;
            hypocentre.Lat = lat;
            return minError;
        }

        public MultiHypocentreMethod()
        {
            _hypocentreErrorMethod = new DispersionErrorMethod();
        }
    }
}