﻿namespace SeismologyClassLibrary
{
    /// <summary>
    /// Представляет скалярное значение для землетрясения
    /// </summary>
    public struct EarthquakeScalar
    {
        /// <summary>
        /// Широта
        /// </summary>
        public double Lat { get; set; }
        /// <summary>
        /// Долгота
        /// </summary>
        public double Lng { get; set; }
        /// <summary>
        /// Значение
        /// </summary>
        public double Value { get; set; }
    }
}