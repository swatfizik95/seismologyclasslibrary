﻿using System;

namespace SeismologyClassLibrary
{
    /// <summary>
    /// Представляет Годограф
    /// </summary>
    public class Hodograph
    {
        /// <summary>
        /// Глубина
        /// </summary>
        public double Depth { get; }
        /// <summary>
        /// Нижнее значение границы времени
        /// </summary>
        public double Tmin { get; }
        /// <summary>
        /// Верхнее значение границы времени
        /// </summary>
        public double Tmax { get; }
        /// <summary>
        /// Уравнение
        /// </summary>
        public Func<double, double> Equation { get; }

        /// <summary>
        /// Инициадизирует новый объект класса
        /// </summary>
        /// <param name="depth">Глубина</param>
        /// <param name="tmin">Начало границы времени</param>
        /// <param name="tmax">Конец границы времени</param>
        /// <param name="equation">Уравнение нахождения эпицентрального расстояния</param>
        public Hodograph(double depth, double tmin, double tmax, Func<double, double> equation)
        {
            Depth = depth;
            Tmin = tmin;
            Tmax = tmax;
            Equation = equation;
        }
    }
}