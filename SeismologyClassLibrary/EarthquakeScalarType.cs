﻿namespace SeismologyClassLibrary
{
    public enum EarthquakeScalarType
    {
        /// <summary>
        /// Плотность
        /// </summary>
        Density,
        /// <summary>
        /// Отношение скоростей
        /// </summary>
        VpVs,
        /// <summary>
        /// Отношение скоростей станционное
        /// </summary>
        StationVpVs,
        /// <summary>
        /// Активность
        /// </summary>
        Activity
    }
}