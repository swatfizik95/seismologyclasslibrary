﻿using System.Collections.Generic;

namespace SeismologyClassLibrary.IO
{
    public interface IEarthquakeReaderCsv
    {
        IEnumerable<Earthquake> ReadCsv(string filePath, string format, char delimiter);
    }
}
