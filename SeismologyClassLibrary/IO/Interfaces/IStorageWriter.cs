﻿namespace SeismologyClassLibrary.IO
{
    public interface IStorageWriter
    {
        void Write(string filePath, string data);
    }
}
