﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Moq;
using NUnit.Framework;
using SeismologyClassLibrary;
using SeismologyClassLibrary.IO;

namespace SeismologyClassLibraryTests.IO
{
    [TestFixture]
    public class EarthquakeFileTests_WithMock
    {
        FileS ef;
        Mock<IStorageWriter> mockWriter;
        Mock<IStorageReader> mockReader;
        FakeEarthquake fake = new FakeEarthquake();
        List<string> storage;
        [SetUp]
        public void Init()
        {
            mockWriter = new Mock<IStorageWriter>();
            mockWriter.Setup(w => w.Write(It.IsAny<string>(), It.IsAny<string>()))
                      .Callback<string, string>((s, str) => {
                          storage.AddRange(str.Split(new[] { Environment.NewLine }, StringSplitOptions.None));
                          //storage.RemoveAt(storage.Count - 1);
                      });

            mockReader = new Mock<IStorageReader>();
            mockReader.Setup(r => r.Open(It.IsAny<string>()))
                      .Callback<string>(s => {});
            mockReader.Setup(r => r.ReadLine())
                      .Returns(() =>
                      {
                          if (storage.Count == 0) return null;
                          string result = storage[0];
                          storage.RemoveAt(0);
                          return result;
                      });
            mockReader.Setup(r => r.Close());
            ef = new FileS(mockReader.Object, mockWriter.Object);
        }
        // Полностью на moq
        [Test]
        public void Read_And_Write_CatalogTxt()
        {
            Earthquake[] fakeEarthquakes = fake.CreateFakeEarthquakes(6).ToArray();

            storage = new List<string>();

            ef.WriteCatalogTxt("somePath", fakeEarthquakes);
            Earthquake[] actualEarthquakes = ef.ReadCatalogTxt("somePath").ToArray();

            CollectionAssert.AreEqual(fakeEarthquakes, actualEarthquakes);
        }

        [Test]
        public void Read_And_Write_BulletinTxt_1000()
        {
            Earthquake[] fakeEarthquakes = fake.CreateFakeEarthquakes(1000, true).ToArray();

            storage = new List<string>();
            Stopwatch sp = new Stopwatch();
            sp.Start();
            ef.WriteBulletinTxt("somePath", fakeEarthquakes);
            Earthquake[] actualEarthquakes = ef.ReadBulletinTxt("somePath").ToArray();
            var count = actualEarthquakes.SelectMany(s => s.Stations).Count();
            sp.Stop();
            Assert.Less(sp.ElapsedMilliseconds, 3000);
        }

        [Test]
        public void Read_And_Write_BulletinTxt_With_Special_Times()
        {
            Earthquake[] fakeEarthquakes =
            {
                new Earthquake(0, new DateTime(2017, 7, 13), new DateTime(2017, 7, 13, 23, 59, 54, 360),
                    3.837, 10.5, 43.198, 46.744, 0.282, 1.597,
                    new List<EarthquakeStation>
                    {
                        new EarthquakeStation("DBC", new DateTime(2017, 7, 13, 23, 59, 56, 340),
                            new DateTime(2017, 7, 13, 23, 59, 59, 710),
                            new DateTime(2017, 7, 14, 0, 0, 09, 800), 21.262, 0.44, 3.92, 1.594,
                            0.007, 0.349, 0.002, 0.6)
                    })
            };


            storage = new List<string>();
            ef.WriteBulletinTxt("somePath", fakeEarthquakes);
            Earthquake[] actualEarthquakes = ef.ReadBulletinTxt("somePath").ToArray();
            CollectionAssert.AreEqual(fakeEarthquakes, actualEarthquakes);
        }

        [Test]
        public void Read_And_Write_BulletinTxt()
        {
            Earthquake[] fakeEarthquakes = fake.CreateFakeEarthquakes(1, true).ToArray();

            storage = new List<string>();

            ef.WriteBulletinTxt("somePath", fakeEarthquakes);
            Earthquake[] actualEarthquakes = ef.ReadBulletinTxt("somePath").ToArray();
            CollectionAssert.AreEqual(fakeEarthquakes, actualEarthquakes);
        }

        //[Test]
        //public void ReadCsv_With_IncorrectTitle()
        //{
        //    storage = new List<string>() {
        //        "МИсяц;День;Коорд;Коорд",
        //        "1;10;42,59;45,27"
        //    };
        //    //Earthquake[] actualEarthquakes = ef.ReadCsv("s").ToArray();
        //    Assert.Throws(Is.TypeOf<FormatException>(),
        //                () => ef.ReadCsv("s").ToArray());
        //}

        //[Test]
        //public void Read_And_Write_Csv()
        //{
        //    Earthquake[] fakeEarthquakes = fake.CreateFakeEarthquakes(6).ToArray();

        //    storage = new List<string>();

        //    ef.WriteCsv("somePath", fakeEarthquakes);
        //    Earthquake[] actualEarthquakes = ef.ReadCsv("somePath").ToArray();

        //    Assert.AreEqual(fakeEarthquakes.Length, actualEarthquakes.Length);
        //    for (int i = 0; i < fakeEarthquakes.Length; i++)
        //    {
        //        Assert.AreEqual(fakeEarthquakes[i].Date,
        //                      actualEarthquakes[i].Date);
        //        Assert.AreEqual(fakeEarthquakes[i].K,
        //                      actualEarthquakes[i].K);
        //        Assert.AreEqual(fakeEarthquakes[i].H,
        //                      actualEarthquakes[i].H);
        //        Assert.AreEqual(fakeEarthquakes[i].Lat,
        //                      actualEarthquakes[i].Lat);
        //        Assert.AreEqual(fakeEarthquakes[i].Lng,
        //                      actualEarthquakes[i].Lng);
                
        //    }
        //    ef.ToString();
        //}
    }
}